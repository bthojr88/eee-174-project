import sys, os, subprocess, signal
from PyQt4 import QtGui, QtCore, QtTest
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import RPi.GPIO as GPIO
import smtplib
import time

to = 'phone@vtext.com'                                  #Replace 'phone' with desired receiving phone number
gmailUser = 'gmail address'                             #Replace 'gmail address' with sending address
password = 'gmail password'                             #Replace 'gmail password' with gmail account password

subject = 'Alert'
txt = 'Raspberry Pi detected an intruder!!!'

#Set GPIO pins
ledPin = 17
piezoPin = 18
motionPin = 27
doorPin = 22
    
GPIO.setmode(GPIO.BCM)                                  #Go by GPIO pin numbers
GPIO.setwarnings(False)

#Initialize GPIO pins as Inputs or Outputs
GPIO.setup(ledPin, GPIO.OUT)
GPIO.setup(piezoPin, GPIO.OUT)
GPIO.setup(motionPin, GPIO.IN)
GPIO.setup(doorPin, GPIO.IN)

#Set initial positions
GPIO.output(ledPin, GPIO.LOW)
GPIO.output(piezoPin, GPIO.LOW)

class Window(QtGui.QWidget):

    def __init__(self, parent=None):
        #Window set up
        super(Window, self).__init__(parent)
        #self.setWindowState(Qt.WindowFullScreen)
        self.setWindowState(Qt.WindowMaximized)
        self.setWindowTitle("Alarm Console")
       
        lblFont = QFont('Times', 50)
        lblFont.setBold(False)
        btnFont = QFont('Times', 26)
        btnFont.setBold(True)
        disableFont = QFont('Times', 16)
        disableFont.setBold(True)
        
        self.win = QtGui.QDialog()
        self.timer = QtCore.QTimer()
        self.playerProcess = subprocess
        
        self.mainLabel = QLabel('Alarm Console', self)
        self.mainLabel.setFont(lblFont)
        self.mainLabel.setAlignment(Qt.AlignCenter)
        
        self.armBtn = QPushButton()
        self.armBtn.setText("UNARMED")
        self.armBtn.setFont(btnFont)
        self.armBtn.setStyleSheet("background-color: green")
        self.armBtn.setFixedHeight(100)
        self.armBtn.setFixedWidth(350)
        self.armBtn.clicked.connect(self.arm)
        
        self.disableBtn = QPushButton()
        self.disableBtn.setText("Disable")
        self.disableBtn.setFont(disableFont)
        self.disableBtn.setFixedHeight(50)
        self.disableBtn.setFixedWidth(150)
        self.disableBtn.setDisabled(True)
        
        self.mediaBtn = QPushButton()
        self.mediaBtn.setText("Music")
        self.mediaBtn.setFixedHeight(30)
        self.mediaBtn.setFixedWidth(100)
        self.mediaBtn.setStyleSheet('background-color: blue')
        
        #Layout setup
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        hbox1 = QHBoxLayout()
        hbox2 = QHBoxLayout()
        
        vbox.addWidget(self.mainLabel)
        vbox.addStretch()
        hbox.addWidget(self.armBtn)
        vbox.addLayout(hbox)
        hbox1.addWidget(self.disableBtn)
        vbox.addLayout(hbox1)
        hbox2.addWidget(self.mediaBtn)
        vbox.addLayout(hbox2)
        vbox.addStretch()
        
        self.setLayout(vbox)
        
        self.disableBtn.clicked.connect(self.passwordDialog)
        self.mediaBtn.clicked.connect(self.playMedia)
        
        self.show()
        
    def arm(self):
        self.armBtn.setText("ARMING")
        #Adjust end of range to desired time before arming system
        for i in range (1,5):
            if i%2 != 0:
                self.armBtn.setStyleSheet("background-color: red")
                GPIO.output(ledPin, GPIO.HIGH)
            else:
                self.armBtn.setStyleSheet("background-color: green")
                GPIO.output(ledPin, GPIO.LOW)
            QtTest.QTest.qWait(750)
        GPIO.output(ledPin, GPIO.HIGH)
        self.armBtn.setText("ARMED")
        self.armBtn.setStyleSheet("background-color: red")
        self.armBtn.setDisabled(True)                                   #Make Arm button unavailable when sysetm is armed
        self.mediaBtn.setDisabled(True)                                 #Make Media button unavailable when sytem is armed
        self.disableBtn.setDisabled(False)
        self.loop()

    def alert(self):
        #Play alarm sound when system is armed and a sensor is triggered
        GPIO.output(ledPin, GPIO.HIGH)
        if GPIO.input(motionPin):
            GPIO.output(ledPin, GPIO.HIGH)
            self.playerProcess = subprocess.Popen(['mpg321','Police_Siren.mp3'])
            #QtTest.QTest.qWait(10000)    
            self.send_mail()
        elif not GPIO.input(doorPin):
            GPIO.output(ledPin, GPIO.HIGH)
            self.playerProcess = subprocess.Popen(['mpg321','Police_Siren.mp3'])
            #QtTest.QTest.qWait(5000)
        else:
            GPIO.output(ledPin, GPIO.LOW)
            GPIO.output(piezoPin, False)

    def loop(self):
        #Continue to wait for an alert signal.
        #Used in place of a While Loop due to PyQt syntax
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.alert)
        self.timer.start(1)
        
    def send_mail(self):
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(gmailUser, password)
        header = 'To:' + to + '\n' + 'From: ' + gmailUser
        header = header + '\n'+ 'Subject: ' + subject + '\n'
        msg = header + '\n' + txt + '\n\n'
        server.sendmail(gmailUser,to,msg)
        server.quit()
        QtTest.QTest.qWait(10000)
        
    def disarm(self):
        GPIO.output(ledPin, GPIO.LOW)
        self.playerProcess.terminate()
        self.armBtn.setText('UNARMED')
        self.armBtn.setStyleSheet('background-color: green')
        self.armBtn.setDisabled(False)
        self.mediaBtn.setDisabled(False)
        self.disableBtn.setDisabled(True)
        self.timer.stop()
        
    
    def passwordDialog(self):
        self.line = QtGui.QLineEdit()
        #self.line.clear()
        
        self.pword = '123456'
        self.lineIn = ''
        self.hidden = '*******************************************'
        self.line.setText(self.hidden)
        
        oneBtn = QPushButton()
        oneBtn.setText('1')
        twoBtn = QPushButton()
        twoBtn.setText('2')
        threeBtn = QPushButton()
        threeBtn.setText('3')
        fourBtn = QPushButton()
        fourBtn.setText('4')
        fiveBtn = QPushButton()
        fiveBtn.setText('5')
        sixBtn = QPushButton()
        sixBtn.setText('6')
        sevenBtn = QPushButton()
        sevenBtn.setText('7')
        eightBtn = QPushButton()
        eightBtn.setText('8')
        nineBtn = QPushButton()
        nineBtn.setText('9')
        zeroBtn = QPushButton()
        zeroBtn.setText('0')
        rtnBtn = QPushButton()
        rtnBtn.setText('ENTER')
        
        vbox = QVBoxLayout()
        hbox1 = QHBoxLayout()
        hbox2 = QHBoxLayout()
        hbox3 = QHBoxLayout()
        hbox4 = QHBoxLayout()
        
        vbox.addWidget(self.line)
        vbox.addStretch()
        
        hbox1.addWidget(oneBtn)
        hbox1.addWidget(twoBtn)
        hbox1.addWidget(threeBtn)
        
        hbox2.addWidget(fourBtn)
        hbox2.addWidget(fiveBtn)
        hbox2.addWidget(sixBtn)
        
        hbox3.addWidget(sevenBtn)
        hbox3.addWidget(eightBtn)
        hbox3.addWidget(nineBtn)
        
        hbox4.addWidget(zeroBtn)
        hbox4.addWidget(rtnBtn)
        
        vbox.addLayout(hbox1)
        vbox.addLayout(hbox2)
        vbox.addLayout(hbox3)
        vbox.addLayout(hbox4)
        
        self.win.setLayout(vbox)
        
        oneBtn.clicked.connect(self.onePress)
        twoBtn.clicked.connect(self.twoPress)
        threeBtn.clicked.connect(self.threePress)
        fourBtn.clicked.connect(self.fourPress)
        fiveBtn.clicked.connect(self.fivePress)
        sixBtn.clicked.connect(self.sixPress)
        sevenBtn.clicked.connect(self.sevenPress)
        eightBtn.clicked.connect(self.eightPress)
        nineBtn.clicked.connect(self.ninePress)
        zeroBtn.clicked.connect(self.zeroPress)
        rtnBtn.clicked.connect(self.rtnPress)
        
        self.win.exec_()
        
    def onePress(self):
        self.lineIn = self.lineIn + '1'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def twoPress(self):
        self.lineIn = self.lineIn + '2'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def threePress(self):
        self.lineIn = self.lineIn + '3'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def fourPress(self):
        self.lineIn = self.lineIn + '4'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def fivePress(self):
        self.lineIn = self.lineIn + '5'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def sixPress(self):
        self.lineIn = self.lineIn + '6'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def sevenPress(self):
        self.lineIn = self.lineIn + '7'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def eightPress(self):
        self.lineIn = self.lineIn + '8'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def ninePress(self):
        self.lineIn = self.lineIn + '9'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def zeroPress(self):
        self.lineIn = self.lineIn + '0'
        #self.hidden = self.hidden + '*'
        #self.line.setText(self.hidden)
    
    def rtnPress(self):
        print(self.pword)
        print(self.lineIn)
        if self.lineIn == self.pword:
            self.lineIn = ''
            #self.hidden = ''
            #self.line.setText(self.hidden)
            self.disarm()
            self.win.close()
        else:
            self.line.setText('Wrong Password')
            self.lineIn = ''
            QtTest.QTest.qWait(1500)
            self.line.setText(self.hidden)
            #self.hidden = ''

    def playMedia(self):
        os.system('vlc --playlist-autostart --loop --playlist-tree ~/Desktop/Music/')
        
def run():
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())

run()

